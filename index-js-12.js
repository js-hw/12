// Question: 1. Чому для роботи з input не рекомендується використовувати клавіатуру?

// Answer: Окрім клавіатури, існує багато способів ввести інформацію у поле input. Наприклад, на мобільних пристроях її можна ввести за допомогою голосу чи дотику, писати по екрану шляхом розпізнавання почерку. На компьютері її можна копіювати та вставляти комбінацією клавіш або за допомогою правої кнопки миши та контекстного меню.

// Для того, щоб відстежувати введення даних користувача у полі input будь-яким способом, краще всього підходить спеціальна подія, яка однойменно назвається input.

// Клавіатурні події краще використовувати для подій, пов'язаних саме з клавіатурою (звичайною чи віртуальною). А також додаткові події, які не взмозі обробити input (наприклад, реагування на комбінації клавіш ("гарячі клавіші"), клавіші зі стрілками ( –> , <– ), Up, Down, функціональні клавіші та ішні).


const btnWrapper = document.querySelectorAll(".btn");

btnWrapper.forEach(function (item) {
  let letter = item.textContent.trim();
  item.setAttribute("data-btn", letter);
});

document.addEventListener("keypress", (event) => {
  let getKey;

  //key – для обох Enter; code – лише для головного Enter
  if (event.key === "Enter") {
    getKey = "Enter";
  } else {
    getKey = event.code.slice(-1);
  }

  let btnCertain = document.querySelector(`.btn[data-btn="${getKey}"]`);

  const isActive = btnCertain.classList.contains("active");

  btnWrapper.forEach((item) => {
    item.classList.remove("active");
  });

  if (btnCertain) {
    btnCertain.classList.toggle("active", !isActive);
  }
});
